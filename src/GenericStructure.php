<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GenericTypes;

/**
 * Class GenericStructure
 * @package Rodziu\GenericTypes
 */
abstract class GenericStructure implements GenericTypeInterface{
	/**
	 * @param array $array
	 *
	 * @param bool $recursive
	 *
	 * @return static
	 */
	public static function fromArray(array $array, bool $recursive = true): GenericTypeInterface{
		$calledClass = get_called_class();
		if($calledClass == self::class){
			$object = new class extends GenericStructure{
			};
			foreach($array as $k => $value){
				$object->$k = $value;
			}
		}else{
			try{
				$reflection = new \ReflectionClass($calledClass);
				$constructor = $reflection->getConstructor();
				if(!is_null($constructor)){
					$arguments = [];
					$parameters = Utility::getMethodParameters($constructor);
					foreach($parameters as $parameter){
						if(isset($array[$parameter['name']])){
							if(
								$recursive
								&& is_subclass_of($parameter['type'], GenericTypeInterface::class)
								&& !($array[$parameter['name']] instanceof GenericTypeInterface)
							){
								/** @noinspection PhpUndefinedMethodInspection */
								$arguments[] = $parameter['type']::fromArray($array[$parameter['name']], $recursive);
							}else{
								$arguments[] = $array[$parameter['name']];
							}
						}else if(array_key_exists('defaultValue', $parameter)){
							$arguments[] = $parameter['defaultValue'];
						}else{
							throw new GenericStructureException(
								"Given array doesn't provide parameter `{$parameter['type']} \${$parameter['name']}`"
								." required by $calledClass constructor."
							);
						}
					}
					$object = new $calledClass(...$arguments);
				}else{
					throw new GenericStructureException("Class $calledClass doesn't have a constructor!", 3);
				}
			}catch(\ReflectionException $e){
				throw new \RuntimeException("Class $calledClass doesn't exist!");
			}
		}
		return $object;
	}

	/**
	 * @param bool $recursive
	 *
	 * @return array
	 */
	public function toArray(bool $recursive = true): array{
		$ret = get_object_vars($this);
		if($recursive){
			foreach($ret as &$r){
				if($r instanceof GenericTypeInterface){
					$r = $r->toArray();
				}
			}
			unset($r);
		}
		return $ret;
	}

	/**
	 * @return \ArrayIterator
	 */
	public function getIterator(): \ArrayIterator{
		return new \ArrayIterator(get_object_vars($this));
	}
}