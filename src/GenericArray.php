<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GenericTypes;

/**
 * Class GenericArray
 * @package Rodziu\GenericTypes
 */
abstract class GenericArray implements GenericTypeInterface, \Countable, \ArrayAccess{
	/**
	 * @var string
	 */
	protected $type = "";
	/**
	 * @var array
	 */
	protected $values = [];
	/**
	 * @var null|string
	 */
	protected $keyType;

	/**
	 * GenericArray constructor.
	 *
	 * @param string|null $type
	 * @param string|null $keyType
	 */
	public function __construct(string $type = null, string $keyType = null){
		$this->type = $type ?? $this->parseType();
		$this->keyType = $keyType;
	}

	/**
	 * @return string
	 */
	private function parseType(): string{
		$class = get_called_class();
		try{
			$reflection = new \ReflectionClass($class);
			try{
				$parameters = Utility::getMethodParameters($reflection->getConstructor());
				$cnt = count($parameters);
				if($cnt){
					return $parameters[$cnt - 1]['type'];
				}
				throw new \Exception();
			}catch(\Throwable $e){
				throw new \LogicException(
					"The last parameter of class $class::__construct (that inherits from ".self::class." should be declared with a splat (...) operator preceded by the type of array elements."
				);
			}
		}catch(\ReflectionException $e){
			throw new \RuntimeException("Class $class doesn't exist!");
		}
	}

	/**
	 * @param array $array
	 * @param bool $recursive
	 *
	 * @return static
	 */
	public static function fromArray(array $array, bool $recursive = true): GenericTypeInterface{
		$calledClass = get_called_class();
		/** @var GenericArray $ret */
		$ret = new $calledClass();
		foreach($array as $k => $v){
			if(
				$recursive
				&& is_subclass_of($ret->getType(), GenericTypeInterface::class)
				&& !($v instanceof GenericTypeInterface)
			){
				/** @noinspection PhpUndefinedMethodInspection */
				$v = $ret->getType()::fromArray($v, $recursive);
			}
			$ret[$k] = $v;
		}
		return $ret;
	}

	/**
	 * @return string
	 */
	public function getType(): string{
		foreach(Utility::TYPE_ALIASES as $t => $alias){
			if($this->type == $t){
				return $alias;
			}
		}
		return $this->type;
	}

	/**
	 * @param bool $recursive
	 *
	 * @return array
	 */
	public function toArray(bool $recursive = true): array{
		$ret = $this->values;
		if($recursive){
			foreach($ret as &$r){
				if($r instanceof GenericStructure || $r instanceof GenericArray){
					$r = $r->toArray();
				}
			}
			unset($r);
		}
		return $ret;
	}

	/**
	 * @return \ArrayIterator
	 */
	public function getIterator(): \ArrayIterator{
		return new \ArrayIterator($this->values);
	}

	/**
	 * @return int
	 */
	public function count(){
		return count($this->values);
	}

	/**
	 * @param mixed $offset
	 * @param mixed $value
	 *
	 * @throws \TypeError
	 */
	public function offsetSet($offset, $value){
		if(Utility::isOfType($value, $this->type)){
			if(is_null($offset)){
				$this->values[] = $value;
			}else if(!is_null($this->keyType) && !Utility::isOfType($offset, $this->keyType)){
				$type = Utility::getValueType($offset);
				throw new \TypeError("Offset must be of the type $this->keyType, $type given.");
			}else{
				$this->values[$offset] = $value;
			}
		}else{
			$type = Utility::getValueType($value);
			throw new \TypeError("Value must be of the type $this->type, $type given.");
		}
	}

	/**
	 * @param mixed $offset
	 *
	 * @return bool
	 */
	public function offsetExists($offset){
		return isset($this->values[$offset]);
	}

	/**
	 * @param mixed $offset
	 */
	public function offsetUnset($offset){
		unset($this->values[$offset]);
	}

	/**
	 * @param mixed $offset
	 *
	 * @return mixed
	 */
	public function offsetGet($offset){
		return $this->values[$offset];
	}

	/**
	 * @return null|string
	 */
	public function getKeyType(): ?string{
		foreach(Utility::TYPE_ALIASES as $t => $alias){
			if($this->keyType == $t){
				return $alias;
			}
		}
		return $this->keyType;
	}
}