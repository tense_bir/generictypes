<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GenericTypes;

/**
 * Class ArrayOfBool
 * @package Rodziu\GenericTypes
 */
class ArrayOfBool extends GenericArray{
	/**
	 * ArrayOfBool constructor.
	 *
	 * @param bool ...$booleans
	 */
	public function __construct(bool ...$booleans){
		parent::__construct('bool');
		$this->values = $booleans;
	}
}