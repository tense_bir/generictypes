<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2018.
 */

namespace Rodziu\GenericTypes;

/**
 * Interface GenericTypeInterface
 * @package Rodziu\GenericTypes
 */
interface GenericTypeInterface extends \IteratorAggregate{
	/**
	 * @param array $array
	 *
	 * @param bool $recursive
	 *
	 * @return static
	 */
	public static function fromArray(array $array, bool $recursive = true): GenericTypeInterface;

	/**
	 * @param bool $recursive
	 *
	 * @return array
	 */
	public function toArray(bool $recursive = true): array;
}