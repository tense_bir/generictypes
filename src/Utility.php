<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GenericTypes;

/**
 * Class Utility
 * @package Rodziu\GenericTypes
 */
abstract class Utility{
	const TYPE_ALIASES = ['integer' => 'int', 'boolean' => 'bool', 'double' => 'float'];
	/**
	 * @param \ReflectionMethod $method
	 *
	 * @return array
	 */
	public static function getMethodParameters(\ReflectionMethod $method): array{
		$ret = [];
		$parameters = $method->getParameters();
		foreach($parameters as $parameter){
			$p = [
				'name' => $parameter->name,
				'type' => $parameter->getType()
			];
			if($parameter->isOptional() && $parameter->isDefaultValueAvailable()){
				try{
					$p['defaultValue'] = $parameter->getDefaultValue();
				}catch(\ReflectionException $e){
				}
			}
			if(!is_null($p['type'])){
				$p['type'] = $p['type']->getName();
			}
			foreach(self::TYPE_ALIASES as $type => $alias){
				if($p['type'] == $type){
					$p['type'] = $alias;
				}
			}
			$ret[] = $p;
		}
		return $ret;
	}

	/**
	 * @param $value
	 *
	 * @return string
	 */
	public static function getValueType($value): string{
		$givenType = gettype($value);
		foreach(self::TYPE_ALIASES as $type => $alias){
			if($givenType == $type){
				$givenType = $alias;
			}
		}
		if($givenType == 'object'){
			$givenType = get_class($value);
		}
		return $givenType;
	}

	/**
	 * @param $value
	 * @param string|null $type
	 *
	 * @return bool
	 */
	public static function isOfType($value, string $type = null): bool{
		if(is_null($type)){
			return true;
		}
		foreach(self::TYPE_ALIASES as $t => $alias){
			if($type == $t){
				$type = $alias;
				break;
			}
		}
		return $value instanceof $type || self::getValueType($value) == $type;
	}
}
