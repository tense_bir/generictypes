<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GenericTypes;

use PHPUnit\Framework\TestCase;

/**
 * Class GenericStructureExtension
 * @package Rodziu\GenericTypes
 */
class GenericStructureExtension extends GenericStructure{
	/**
	 * @var string
	 */
	private $private;
	/**
	 * @var string
	 */
	protected $protected;
	/**
	 * @var int
	 */
	public $int;
	/**
	 * @var float
	 */
	public $float;
	/**
	 * @var string
	 */
	public $string;
	/**
	 * @var mixed
	 */
	public $object;
	/**
	 * @var array
	 */
	public $array;

	/**
	 * GenericStructureExtension constructor.
	 *
	 * @param int $int
	 * @param float $float
	 * @param string $string
	 * @param $object
	 * @param array $array
	 */
	public function __construct(int $int, float $float, string $string, $object, array $array = null){
		$this->int = $int;
		$this->float = $float;
		$this->string = $string;
		$this->object = $object;
		$this->private = "private property";
		$this->protected = "protected property";
		$this->array = $array ?? [];
	}
}
/**
 * Class NoConstructStructure
 * @package Rodziu\GenericTypes
 */
class NoConstructStructure extends GenericStructure{
}
/**
 * Class NestedGenericStructure
 * @package Rodziu\GenericTypes
 */
class NestedGenericStructure extends GenericStructure{
	/**
	 * @var GenericStructureExtension
	 */
	public $object;

	/**
	 * NestedGenericStructure constructor.
	 *
	 * @param GenericStructureExtension $object
	 */
	public function __construct(GenericStructureExtension $object){
		$this->object = $object;
	}
}
/**
 * Class GenericStructureTest
 * @package Rodziu\GenericTypes
 */
class GenericStructureTest extends TestCase{
	/**
	 * @param $object
	 *
	 * @return GenericStructureExtension
	 */
	private function getMockStruct($object){
		return new GenericStructureExtension(1, 2.2, "text", $object);
	}

	/**
	 * @dataProvider fromArrayDataProvider
	 *
	 * @param string $class
	 * @param array $argument
	 * @param array $expectedAttributes
	 * @param string|null $exceptionRegex
	 *
	 * @throws GenericStructureException
	 */
	public function testFromArray(string $class, array $argument, array $expectedAttributes, string $exceptionRegex = null){
		if(!is_null($exceptionRegex)){
			$this->expectExceptionMessageRegExp($exceptionRegex);
		}
		/** @var GenericStructure $class */
		$object = $class::fromArray($argument);
		foreach($expectedAttributes as $k => $v){
			$this->assertEquals($v, $object->$k);
		}
	}

	/**
	 * @return array
	 */
	public function fromArrayDataProvider(): array{
		$stdClass = new \stdClass();
		$construct = ['int' => 1, 'float' => 2.2, 'string' => 'text', 'object' => $stdClass, 'array' => []];
		$constructWithoutOptional = $construct;
		unset($constructWithoutOptional['array']);
		$badType = $construct;
		$badType['int'] = '';
		return [
			'wrong type'                        => [
				GenericStructureExtension::class, $badType, [], '#Argument 1 passed to.*must be of the type integer, string given#'
			],
			'anonymous class'                      => [GenericStructure::class, ['a' => 1, 'b' => 2], ['a' => 1, 'b' => 2]],
			'structure'                            => [
				GenericStructureExtension::class, array_merge($construct, ['aaa' => '1']), $construct
			],
			'structure with no argument'           => [
				GenericStructureExtension::class, array_merge($construct, ['float' => '']), [],
				'#float, string given#'
			],
			'bad construct'                        => [
				GenericStructureExtension::class, [], [], '#parameter `int \$int`#'
			],
			'construct without optional parameter' => [
				GenericStructureExtension::class, $constructWithoutOptional, $construct
			],
			'no construct'                         => [
				NoConstructStructure::class, [], [], '##'
			],
			'recursive'                            => [
				NestedGenericStructure::class, ['object' => $construct], ['object' => $this->getMockStruct($stdClass)]
			],
		];
	}

	/**
	 */
	public function testToArray(){
		$stdClass = new \stdClass();
		$objectAsArray = [
			'int' => 1, 'float' => 2.2, 'string' => 'text', 'object' => $stdClass, 'array' => [], 'protected' => 'protected property'
		];
		$this->assertEquals(
			$objectAsArray,
			$this->getMockStruct($stdClass)->toArray()
		);
		$nested = new NestedGenericStructure($this->getMockStruct($stdClass));
		$this->assertEquals(
			['object' => $objectAsArray],
			$nested->toArray(true)
		);
	}

	/**
	 */
	public function testIterator(){
		$stdClass = new \stdClass();
		self::assertSame(
			[
				'protected' => 'protected property', 'int' => 1, 'float' => 2.2, 'string' => 'text', 'object' => $stdClass, 'array' => []
			],
			iterator_to_array($this->getMockStruct($stdClass))
		);
	}

	/**
	 */
	public function testSerializable(){
		$stdClass = new \stdClass();
		$struct = $this->getMockStruct($stdClass);
		$unSerialized = unserialize(serialize($struct));
		$this->assertEquals($struct, $unSerialized);
	}
}
