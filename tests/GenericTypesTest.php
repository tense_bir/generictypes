<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GenericTypes;

use PHPUnit\Framework\TestCase;

/**
 * Class GenericTypesTest
 * @package Rodziu\GenericTypes
 */
class GenericTypesTest extends TestCase{
	/**
	 * @dataProvider dataProvider
	 *
	 * @param string $className
	 * @param bool $willPass
	 * @param $expected
	 * @param array ...$arguments
	 *
	 * @internal param bool $ret
	 */
	public function testTypes(string $className, bool $willPass, $expected, ...$arguments){
		if(!$willPass){
			$this->expectException(\TypeError::class);
		}
		/** @var GenericArray $object */
		$object = new $className(...$arguments);
		$this->assertEquals($expected, $object->toArray());
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function dataProvider(): array{
		$tests = $ret = [];
		$tests[ArrayOfBool::class] = [
			'empty'   => [true, []],
			'cast'    => [true, [true], 1],
			'cast2'   => [true, [true], 231],
			'cast3'   => [true, [true], -1],
			'invalid' => [false, [], new \stdClass],
			'valid'   => [true, [true, false], true, false]
		];
		$function = function(){};
		$tests[ArrayOfCallable::class] = [
			'empty'   => [true, []],
			'invalid' => [false, [], new \stdClass],
			'valid'   => [true, [$function], $function]
		];
		$dateTime = new \DateTime();
		$tests[ArrayOfDateTime::class] = [
			'empty'   => [true, []],
			'invalid' => [false, [], 1],
			'valid'   => [true, [$dateTime], $dateTime]
		];
		$tests[ArrayOfFloat::class] = [
			'empty'   => [true, []],
			'cast'    => [true, [1.1], '1.1'],
			'invalid' => [false, [], new \stdClass],
			'valid'   => [true, [1, 2.2, 3.3], 1, 2.2, 3.3]
		];
		$tests[ArrayOfInt::class] = [
			'empty'   => [true, []],
			'cast'    => [true, [1], 1.1],
			'invalid' => [false, [], new \stdClass],
			'valid'   => [true, [1, 2, 3], 1, 2, 3]
		];
		$tests[ArrayOfString::class] = [
			'empty'   => [true, []],
			'cast'    => [true, ['1'], 1],
			'invalid' => [false, [], new \stdClass],
			'valid'   => [true, ['string'], 'string']
		];
		//
		foreach($tests as $class => $data){
			$className = preg_replace("#^.*\\\\#", '', $class);
			foreach($data as $k => $v){
				$ret["$k $className"] = array_merge([$class], $v);
			}
		}
		return $ret;
	}
}
