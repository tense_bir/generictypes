<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GenericTypes;

use PHPUnit\Framework\TestCase;

/**
 * Class UtilityTest
 * @package Rodziu\GenericTypes
 */
class UtilityTest extends TestCase{
	/**
	 */
	public function empty(){
	}

	/**
	 * @param int $int
	 * @param bool $bool
	 * @param $noType
	 */
	public function func(int $int, bool $bool, $noType){
	}

	/**
	 * @throws \ReflectionException
	 */
	public function testGetMethodParameters(){
		/** @noinspection PhpUnhandledExceptionInspection */
		$reflection = new \ReflectionClass(self::class);
		$parameters = Utility::getMethodParameters($reflection->getMethod('empty'));
		$this->assertEquals([], $parameters);
		$parameters = Utility::getMethodParameters($reflection->getMethod('func'));
		$this->assertEquals([
			['name' => 'int', 'type' => 'int'],
			['name' => 'bool', 'type' => 'bool'],
			['name' => 'noType', 'type' => null]
		], $parameters);
	}

	/**
	 * @dataProvider getValueTypeDataProvider
	 *
	 * @param string $expected
	 * @param $value
	 */
	public function testGetValueType(string $expected, $value){
		$this->assertEquals($expected, Utility::getValueType($value));
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function getValueTypeDataProvider(): array{
		return [
			'string'   => ['string', ''],
			'integer'  => ['int', 1],
			'float'    => ['float', 1.1],
			'bool'     => ['bool', false],
			'closure'  => ['Closure', function(){
			}],
			'DateTime' => ['DateTime', new \DateTime()],
			'array'    => ['array', []]
		];
	}

	/**
	 * @dataProvider isOfTypeDataProvider
	 *
	 * @param bool $expected
	 * @param $value
	 * @param string $type
	 */
	public function testIsOfType(bool $expected, $value, string $type){
		$this->assertEquals($expected, Utility::isOfType($value, $type));
	}

	/**
	 * @return array
	 */
	public function isOfTypeDataProvider(): array{
		return [
			'int'               => [true, 1, 'int'],
			'integer'           => [true, 1, 'integer'],
			'string is not int' => [false, '', 'integer']
		];
	}
}
