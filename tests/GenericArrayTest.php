<?php
/**
 *    ______                     _         ______
 *   / ____/__  ____  ___  _____(_)____   /_  __/_  ______  ___  _____
 *  / / __/ _ \/ __ \/ _ \/ ___/ / ___/    / / / / / / __ \/ _ \/ ___/
 * / /_/ /  __/ / / /  __/ /  / / /__     / / / /_/ / /_/ /  __(__  )
 * \____/\___/_/ /_/\___/_/  /_/\___/    /_/  \__, / .___/\___/____/
 *                                           /____/_/
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @copyright Copyright (c) 2017.
 */

namespace Rodziu\GenericTypes;

use PHPUnit\Framework\{
	Error\Notice, TestCase
};

/**
 * Class GenericArrayExtension
 * @package Rodziu\GenericTypes
 */
class GenericArrayExtension extends GenericArray{
	public function __construct(string ...$values){
		parent::__construct();
		$this->values = $values;
	}
}
/**
 * Class TestArray
 * @package Rodziu\GenericTypes
 */
class TestArray extends GenericArray{
	public function __construct(GenericArrayExtension ...$values){
		parent::__construct();
		$this->values = $values;
	}
}
/**
 * Class GenericArrayTest
 * @package Rodziu\GenericTypes
 */
class GenericArrayTest extends TestCase{
	/**
	 */
	const ARRAY = ['a', 'b', 'c'];
	/**
	 * @var GenericArray
	 */
	private $object;

	/**
	 */
	protected function setUp(){
		parent::setUp();
		$this->object = new GenericArrayExtension(...GenericArrayTest::ARRAY);
	}

	/**
	 */
	public function testConstruct(){
		$this->assertEquals(self::ARRAY, iterator_to_array($this->object));
	}

	/**
	 */
	public function testSerializable(){
		$serialized = serialize($this->object);
		$unSerialized = unserialize($serialized);
		$this->assertEquals($this->object, $unSerialized);
	}

	/**
	 */
	public function testCountable(){
		$this->assertEquals(count(self::ARRAY), count($this->object));
	}

	/**
	 */
	public function testArrayAccess(){
		$this->assertTrue(isset($this->object[1]));
		$this->assertFalse(isset($this->object[4]));
		unset($this->object[1]);
		$this->assertFalse(isset($this->object[1]));
		$this->object[] = 'test';
		$this->assertEquals('test', $this->object[3]);
		$this->expectException(Notice::class);
		$this->object[4];
	}

	/**
	 */
	public function testArrayPush(){
		$this->expectException(\TypeError::class);
		$this->object[] = 1;
	}

	/**
	 *
	 */
	public function testKeyType(){
		$obj = new class extends GenericArray{
			public function __construct(){
				parent::__construct('string', 'int');
			}
		};
		$obj[1] = 'test';
		self::assertSame([1 => 'test'],$obj->toArray());
		$this->expectException(\TypeError::class);
		$obj['test'] = '2';
	}

	/**
	 */
	public function testInvalidOffsetSet(){
		$this->expectException(\TypeError::class);
		$this->expectExceptionMessage("Value must be of the type string, stdClass given");
		$this->object[] = new \stdClass();
	}

	/**
	 */
	public function testToArray(){
		$obj = new GenericArrayExtension('test', 'test1', 'test2');
		$this->assertSame(['test', 'test1', 'test2'], $obj->toArray());
		$obj = new TestArray($obj);
		$this->assertSame([['test', 'test1', 'test2']], $obj->toArray());
	}

	/**
	 */
	public function testFromArray(){
		$obj = GenericArrayExtension::fromArray(['test', 'test1', 'test2']);
		$this->assertSame(['test', 'test1', 'test2'], $obj->toArray());
		self::assertInstanceOf(GenericArrayExtension::class, $obj);
		// recursive
		$obj = TestArray::fromArray([['test', 'test1', 'test2']]);
		self::assertInstanceOf(TestArray::class, $obj);
		$this->assertSame([['test', 'test1', 'test2']], $obj->toArray());
		foreach($obj as $sub){
			self::assertInstanceOf(GenericArrayExtension::class, $sub);
		}
	}
}
